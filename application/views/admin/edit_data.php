<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><i class="fas fa-edit"></i>Edit Data</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <?php foreach($pembeli as $pbl) : ?>
        <form method="post"action="<?php echo base_url().'admin/data_pelanggan/update'?>">

        <div class="form-group">
          <label>id Pembeli</label>
          <input type="text" name="id_pembeli" class="form-control" value="<?php echo $pbl->id_pembeli ?>">
        </div>
        <div class="form-group">
          <label>Nama Pembeli</label>
          <input type="text" name="nama_pembeli" class="form-control" value="<?php echo $pbl->nama_pembeli ?>">
        </div>
        <div class="form-group">
          <label>email</label>
          <input type="text" name="email" class="form-control" value="<?php echo $pbl->email ?>">
        </div>
        <div class="form-group">
          <label>no hp</label>
          <input type="text" name="no_hp" class="form-control" value="<?php echo $pbl->no_hp ?>">
        </div>
        <div class="form-group">
          <label>alamat</label>
          <input type="text" name="alamat" class="form-control" value="<?php echo $pbl->alamat ?>">
        </div>
        <div class="form-group">
          <label>bukti bayar</label>
          <input type="text" name="buktibayar_url" class="form-control" value="<?php echo $pbl->buktibayar_url ?>">
        </div>
        <div class="form-group">
          <label>jenis pengiriman</label><br>
          <select class="form-control" name="jenispengiriman" class="form-control" value="<?php echo $pbl->jenispengiriman ?>">
                <option selected disabled>-- Pilih Kategori --</option>
                <option>luarkota</option>
                <option>dalamkota</option>
            </select>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success">Simpan</button>
      </div>
      </form>
    </div>
    <?php endforeach; ?>
  </div>
  </section>
</div>
