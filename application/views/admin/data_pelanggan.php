<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Pelanggan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Admin</a></li>
              <li class="breadcrumb-item active">Data Pelanggan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

 <!-- Main content -->
<div class="container-fluid">
    <?php echo $this->session->flashdata('message'); ?>
    <button id="btn" class="btn btn-sm btn-primary mb-3" data-toggle="modal" data-target="#exampleModal">
        <i class="fas fa-plus fa-sm"></i> Tambah Data</button>

        <div class="card">
              <!-- /.card-header -->
              <div class="card-body table-bordered p-0">
                <table class="table table-hover">
                  <thead>
                  <tr>
                    <th>Id Pembeli</th>
                    <th>Nama Pembeli</th>
                    <th>Email</th>
                    <th>No HP</th>
                    <th>Alamat</th>
                    <th>Bukti Bayar</th>
                    <th>Jenis Pengiriman</th>
                    <th colspan="3"></th>
                  </tr>
                  </thead>
        
        <?php 
        $no=1;
        foreach($pembeli as $pbl): ?>
            <tr>
                <td><?php echo $pbl->id_pembeli ?></td>
                <td><?php echo $pbl->nama_pembeli ?></td>
                <td><?php echo $pbl->email ?></td>
                <td><?php echo $pbl->no_hp ?></td>
                <td><?php echo $pbl->alamat?></td>
                <td><?php echo $pbl->buktibayar_url ?></td>
                <td><?php echo $pbl->jenispengiriman ?></td>
                <td><?php echo anchor('admin/data_pelanggan/edit/'.$pbl->id_pembeli, '<div class="btn btn-primary btn-sm"><i class="fa fa-edit"></li></div>') ?></td>
                <td onclick="javascript: return confirm('Anda yakin ingin menghapus data ini?')">
                <?php echo anchor('admin/data_pelanggan/hapus/'.$pbl->id_pembeli, '<div class="btn btn-danger btn-sm"><i class="fa fa-trash"></li></div>') ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?php echo base_url().'admin/data_pelanggan/tambah_aksi'; ?>" method="post" enctype="multipart/form-data">
        <div class="form-group">
          <label>Nama Pembeli</label>
          <input type="text" name="nama_pembeli" class="form-control">
        </div>
        <div class="form-group">
          <label>Email</label>
          <input type="text" name="email" class="form-control">
        </div>
        <div class="form-group">
          <label>No HP</label>
          <input type="text" name="no_hp" class="form-control">
        </div>
        <div class="form-group">
          <label>Alamat</label>
          <input type="text" name="alamat" class="form-control">
        </div>
        <div class="form-group">
          <label>Bukti Bayar</label>
          <input type="text" name="buktibayar_url" class="form-control">
        </div>
        <div class="form-group">
          <label>Jenis Pengiriman</label><br>
          <select class="form-control" name="jenispengiriman" class="form-control" value="<?php echo $pbl->jenispengiriman ?>">
                <option selected disabled>-- Pilih Kategori --</option>
                <option>luarkota</option>
                <option>dalamkota</option>
            </select>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>