<?php

class Dashboard_admin extends CI_Controller{
    public function index()
    {
        $this->load->view('admin_temp/header');
        $this->load->view('admin_temp/sidebar');
        $this->load->view('admin/dashboard_admin');
        $this->load->view('admin_temp/footer');
    }
}