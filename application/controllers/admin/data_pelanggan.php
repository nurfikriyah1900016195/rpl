<?php

class Data_pelanggan extends CI_Controller{
    public function index()
    {
        $data['pembeli'] = $this->pembeli_model->tampil_data()->result();
        $this->load->view('admin_temp/header');
        $this->load->view('admin_temp/sidebar');
        $this->load->view('admin/data_pelanggan',$data);
        $this->load->view('admin_temp/footer');
    }
    
    public function tambah_aksi()
    {
        $id_pembeli        = $this->input->post('id_pembeli');
        $nama_pembeli      = $this->input->post('nama_pembeli');
        $email             = $this->input->post('email');
        $no_hp             = $this->input->post('no_hp');
        $alamat            = $this->input->post('alamat');
        $buktibayar_url    = $this->input->post('buktibayar_url');
        $jenispengiriman   = $this->input->post('jenispengiriman');

        $data = array(
          'id_pembeli'           => $id_pembeli,
          'nama_pembeli'         => $nama_pembeli,
          'email'                => $email,
          'no_hp'                => $no_hp,
          'alamat'               => $alamat,
          'buktibayar_url '      => $buktibayar_url ,
          'jenispengiriman'      => $jenispengiriman
        );
        $this->pembeli_model->tambah_data($data, 'pembeli');
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
        Data Berhasil Ditambahkan
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>');

        redirect('admin/data_pelanggan/index');
    }

    public function edit($id_pembeli)
    {
        $where = array('id_pembeli' =>$id_pembeli  );

        $data['pembeli'] = $this->pembeli_model->edit_data($where,'pembeli')->result();
        $this->load->view('admin_temp/header');
        $this->load->view('admin_temp/sidebar');
        $this->load->view('admin/edit_data',$data);
        $this->load->view('admin_temp/footer');
    }

    public function update()
    {
      $id_pembeli        = $this->input->post('id_pembeli');
      $nama_pembeli      = $this->input->post('nama_pembeli');
      $email             = $this->input->post('email');
      $no_hp             = $this->input->post('no_hp');
      $alamat            = $this->input->post('alamat');
      $buktibayar_url    = $this->input->post('buktibayar_url');
      $jenispengiriman   = $this->input->post('jenispengiriman');

        $data = array(
          'id_pembeli'           => $id_pembeli,
          'nama_pembeli'         => $nama_pembeli,
          'email'                => $email,
          'no_hp'                => $no_hp,
          'alamat'               => $alamat,
          'buktibayar_url '      => $buktibayar_url,
          'jenispengiriman'      => $jenispengiriman
      );
        $where = array(
            'id_pembeli'    => $id_pembeli  
        );

        $this->pembeli_model->update_data($where,$data,'pembeli');
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
        Data Berhasil Diupdate
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>');
        redirect('admin/data_pelanggan/index');
    }

    public function hapus($id_pembeli)
    {
        $where = array('id_pembeli' => $id_pembeli);
        $this->pembeli_model->delete_data($where, 'pembeli');
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
        Data Berhasil Dihapus
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>');
        redirect('admin/data_pelanggan/index');
    }
}